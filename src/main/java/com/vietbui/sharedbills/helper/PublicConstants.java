package com.vietbui.sharedbills.helper;

import java.text.SimpleDateFormat;

public class PublicConstants {

	public static final String DATA_SEPARATOR = ",";

	public static final SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat("dd-MM-yyyy");

}

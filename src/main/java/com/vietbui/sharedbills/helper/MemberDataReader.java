package com.vietbui.sharedbills.helper;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;

import com.vietbui.sharedbills.model.Holiday;
import com.vietbui.sharedbills.model.Member;

public class MemberDataReader {

	public static final List<Member> readMemberDataFromFile(final File memberDataFile) {

		if (memberDataFile == null) {
			throw new IllegalArgumentException("Member data file is null");
		}

		if (!memberDataFile.exists()) {
			throw new IllegalArgumentException("Member data file does not exist");
		}

		try {
			@SuppressWarnings("unchecked")
			final List<String> memberRecords = FileUtils.readLines(memberDataFile, "UTF-8");

			final Map<String, Member> members = new HashMap<String, Member>();

			// Parse member record one by one
			for (int i = 0; i < memberRecords.size(); i++) {
				final String memberRecord = memberRecords.get(i);

				final String[] memberDetails = memberRecord.split(PublicConstants.DATA_SEPARATOR);

				if (memberDetails.length != 1 && memberDetails.length != 3) {
					throw new IllegalArgumentException("Invalid member record");
				}

				final String memberName = memberDetails[0];
				final Member member;
				if (members.containsKey(memberName)) {
					member = members.get(memberName);
				}
				else {
					member = new Member();
					member.setName(memberName);
					member.setHolidays(new ArrayList<Holiday>());
					members.put(memberName, member);
				}

				if (memberDetails.length == 3) {
					final Holiday holiday = new Holiday();
					holiday.setStartDate(PublicConstants.SIMPLE_DATE_FORMAT.parse(memberDetails[1]));
					holiday.setEndDate(PublicConstants.SIMPLE_DATE_FORMAT.parse(memberDetails[2]));

					member.getHolidays().add(holiday);
				}
			}

			final List<Member> result;
			if (members.values() instanceof List)
				result = (List<Member>) members.values();
			else
				result = new ArrayList<Member>(members.values());

			// Sort member alphabetically
			Collections.sort(result);

			return result;
		}
		catch (IOException e) {
			throw new IllegalArgumentException("Invalid member data file");
		}
		catch (ParseException e) {
			throw new IllegalArgumentException("Invalid member data record");
		}
	}

}

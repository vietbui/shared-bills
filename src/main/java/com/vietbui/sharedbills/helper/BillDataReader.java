package com.vietbui.sharedbills.helper;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.io.FileUtils;

import com.vietbui.sharedbills.model.Bill;

public class BillDataReader {

	public static final List<Bill> readBillDataFromFile(final File billDataFile) {

		if (billDataFile == null) {
			throw new IllegalArgumentException("Bill data file is null");
		}

		if (!billDataFile.exists()) {
			throw new IllegalArgumentException("Bill data file does not exist");
		}

		try {
			@SuppressWarnings("unchecked")
			final List<String> billRecords = FileUtils.readLines(billDataFile, "UTF-8");

			final List<Bill> result = new ArrayList<Bill>();

			// Parse bill record one by one
			for (int i = 0; i < billRecords.size(); i++) {
				final String billRecord = billRecords.get(i);

				final String[] billDetails = billRecord.split(PublicConstants.DATA_SEPARATOR);

				if (billDetails.length != 4) {
					throw new IllegalArgumentException("Invalid bill record");
				}

				final Bill bill = new Bill();
				bill.setType(billDetails[0]);
				bill.setStartDate(PublicConstants.SIMPLE_DATE_FORMAT.parse(billDetails[1]));
				bill.setEndDate(PublicConstants.SIMPLE_DATE_FORMAT.parse(billDetails[2]));
				bill.setTotal(Float.parseFloat(billDetails[3]));

				result.add(bill);
			}

			// Short bills by type and date
			Collections.sort(result);

			return result;
		}
		catch (IOException e) {
			throw new IllegalArgumentException("Invalid bill data file");
		}
		catch (ParseException e) {
			throw new IllegalArgumentException("Invalid bill data record");
		}
	}

}

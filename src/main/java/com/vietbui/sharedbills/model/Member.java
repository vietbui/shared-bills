package com.vietbui.sharedbills.model;

import java.util.List;

public class Member implements Comparable<Member> {

	private String name;

	private List<Holiday> holidays;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Holiday> getHolidays() {
		return holidays;
	}

	public void setHolidays(List<Holiday> holidays) {
		this.holidays = holidays;
	}

	@Override
	public int compareTo(Member o) {
		return this.name.compareTo(o.getName());
	}

}

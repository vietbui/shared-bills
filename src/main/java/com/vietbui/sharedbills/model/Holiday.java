package com.vietbui.sharedbills.model;

import java.util.Date;

public class Holiday implements Comparable<Holiday> {

	private Date startDate;

	private Date endDate;

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	@Override
	public int compareTo(Holiday o) {
		return this.startDate.compareTo(o.getStartDate());
	}

}

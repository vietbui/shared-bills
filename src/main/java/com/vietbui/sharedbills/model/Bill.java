package com.vietbui.sharedbills.model;

import java.util.Date;

public class Bill implements Comparable<Bill> {

	// Bill type (Electricity, Water, Gas...)
	private String type;

	private Date startDate;

	private Date endDate;

	private float total;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public float getTotal() {
		return total;
	}

	public void setTotal(float total) {
		this.total = total;
	}

	@Override
	public int compareTo(Bill o) {
		if (this.type.equals(o.getType())) {
			return this.startDate.compareTo(o.getStartDate());
		}
		else {
			return this.type.compareTo(o.getType());
		}
	}

}

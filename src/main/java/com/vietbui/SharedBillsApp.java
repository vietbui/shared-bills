package com.vietbui;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.vietbui.sharedbills.helper.BillDataReader;
import com.vietbui.sharedbills.helper.MemberDataReader;
import com.vietbui.sharedbills.helper.PublicConstants;
import com.vietbui.sharedbills.model.Bill;
import com.vietbui.sharedbills.model.Holiday;
import com.vietbui.sharedbills.model.Member;

/**
 * Hello world!
 * 
 */
public class SharedBillsApp {

	public static void main(String[] args) throws FileNotFoundException, UnsupportedEncodingException {
		// Validate arguments
		validateArgurements(args);

		// Read member data file
		final List<Member> members = MemberDataReader.readMemberDataFromFile(new File(args[0]));

		// Read bill data file
		final List<Bill> bills = BillDataReader.readBillDataFromFile(new File(args[1]));

		// Write shared bills summary to file
		writeSharedBillsSummary(new File(args[2]), members, bills);
	}

	private static void validateArgurements(final String[] args) {
		// Check number of arguments
		if (args.length != 3) {
			throw new IllegalArgumentException("Invalid number of arguments");
		}

		final File memberDataFile = new File(args[0]);
		// Check if member data file exist
		if (!memberDataFile.exists()) {
			throw new IllegalArgumentException("Member data file does not exist");
		}

		final File billDataFile = new File(args[1]);
		// Check if bill data file exist
		if (!billDataFile.exists()) {
			throw new IllegalArgumentException("Bill data file does not exist");
		}
	}

	private static void writeSharedBillsSummary(final File summaryOutputFile, final List<Member> members,
			final List<Bill> bills) throws FileNotFoundException, UnsupportedEncodingException {
		final PrintWriter writer = new PrintWriter(summaryOutputFile, "UTF-8");

		writer.println("======================================================================");
		writer.println("\tSHARED BILLS SUMMARY");
		writer.println();
		writer.println("======================================================================");
		writer.println("\tHOLIDAYS SUMMARY");
		writer.println();
		for (final Member member : members) {
			writer.println("\t\t" + member.getName());
			if (member.getHolidays().isEmpty()) {
				writer.println("\t\t\tNo holiday recorded");
			}
			else {
				for (final Holiday holiday : member.getHolidays()) {
					writer.println("\t\t\t" + PublicConstants.SIMPLE_DATE_FORMAT.format(holiday.getStartDate()) + " - "
							+ PublicConstants.SIMPLE_DATE_FORMAT.format(holiday.getEndDate()));
				}
			}
		}
		writer.println("======================================================================");
		writer.println("\tBILLS DETAILS SUMMARY");
		writer.println();

		final Map<String, Float> totalPayment = new HashMap<String, Float>();
		for (int i = 0; i < bills.size(); i++) {
			final Bill bill = bills.get(i);

			writer.println((i + 1) + ".\t" + bill.getType() + " ("
					+ PublicConstants.SIMPLE_DATE_FORMAT.format(bill.getStartDate()) + " - "
					+ PublicConstants.SIMPLE_DATE_FORMAT.format(bill.getEndDate()) + ") "
					+ daysBetween(bill.getStartDate(), bill.getEndDate()) + " days - $" + bill.getTotal());

			int totalBilledPersonDays = 0;

			for (final Member member : members) {
				totalBilledPersonDays += getCountedDays(bill.getStartDate(), bill.getEndDate(), member.getHolidays());
			}

			final float costPerDay = (float) bill.getTotal() / totalBilledPersonDays;

			for (final Member member : members) {
				final int countedDays = getCountedDays(bill.getStartDate(), bill.getEndDate(), member.getHolidays());
				final float payment = costPerDay * countedDays;

				if (totalPayment.containsKey(member.getName())) {
					totalPayment.put(member.getName(), totalPayment.get(member.getName()) + payment);
				}
				else {
					totalPayment.put(member.getName(), payment);
				}

				writer.println("\t" + member.getName() + "\t" + countedDays + " days\t$" + payment);
			}
			writer.println();
		}
		writer.println("======================================================================");
		writer.println("\tPAYMENT SUMMARY");
		writer.println();
		for (final Member member : members) {
			writer.println("\t\t" + member.getName() + "\t$" + totalPayment.get(member.getName()));
		}
		writer.println("======================================================================");
		writer.close();

	}

	private static int daysBetween(Date startDate, Date endDate) {
		return (int) ((endDate.getTime() - startDate.getTime()) / (1000 * 60 * 60 * 24));
	}

	private static int getCountedDays(final Date startDate, final Date endDate, final List<Holiday> holidays) {
		int countedDays = daysBetween(startDate, endDate);

		for (final Holiday holiday : holidays) {
			if (holiday.getEndDate().before(startDate))
				continue;
			else if (holiday.getEndDate().before(endDate)) {
				if (holiday.getStartDate().before(startDate))
					countedDays -= daysBetween(startDate, holiday.getEndDate());
				else
					countedDays -= daysBetween(holiday.getStartDate(), holiday.getEndDate());
			}
			else {
				if (holiday.getStartDate().before(startDate))
					countedDays = 0;
				else if (holiday.getStartDate().before(endDate))
					countedDays -= daysBetween(holiday.getStartDate(), endDate);
				else
					continue;
			}
		}

		return countedDays;
	}

}

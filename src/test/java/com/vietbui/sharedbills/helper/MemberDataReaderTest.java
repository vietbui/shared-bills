package com.vietbui.sharedbills.helper;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.util.List;

import org.junit.Test;

import com.vietbui.sharedbills.model.Member;

public class MemberDataReaderTest {

	private static final String MEMBER_DATA_FILE_VALID = "src/test/resources/members.csv";

	@Test
	public void should_read_member_data_correctly() {
		final List<Member> members = MemberDataReader.readMemberDataFromFile(new File(MEMBER_DATA_FILE_VALID));
		assertEquals(4, members.size());
		assertEquals("Kien", members.get(0).getName());
		assertEquals("Nhung", members.get(1).getName());

		assertEquals("Tuan", members.get(2).getName());
		assertEquals(2, members.get(2).getHolidays().size());
		assertEquals("12-03-2012",
				PublicConstants.SIMPLE_DATE_FORMAT.format(members.get(2).getHolidays().get(0).getStartDate()));
		assertEquals("25-03-2012",
				PublicConstants.SIMPLE_DATE_FORMAT.format(members.get(2).getHolidays().get(0).getEndDate()));
		assertEquals("23-07-2013",
				PublicConstants.SIMPLE_DATE_FORMAT.format(members.get(2).getHolidays().get(1).getStartDate()));
		assertEquals("22-08-2013",
				PublicConstants.SIMPLE_DATE_FORMAT.format(members.get(2).getHolidays().get(1).getEndDate()));

		assertEquals("Viet", members.get(3).getName());
		assertEquals(1, members.get(3).getHolidays().size());
		assertEquals("01-01-2013",
				PublicConstants.SIMPLE_DATE_FORMAT.format(members.get(3).getHolidays().get(0).getStartDate()));
		assertEquals("07-02-2013",
				PublicConstants.SIMPLE_DATE_FORMAT.format(members.get(3).getHolidays().get(0).getEndDate()));
	}

}

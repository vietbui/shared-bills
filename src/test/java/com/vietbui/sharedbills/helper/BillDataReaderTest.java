package com.vietbui.sharedbills.helper;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.util.List;

import org.junit.Test;

import com.vietbui.sharedbills.model.Bill;

public class BillDataReaderTest {

	private static final String BILL_DATA_FILE_VALID = "src/test/resources/bills.csv";

	@Test
	public void should_read_bill_data_correctly() {
		final List<Bill> bills = BillDataReader.readBillDataFromFile(new File(BILL_DATA_FILE_VALID));

		assertEquals(7, bills.size());

		assertEquals("Electricity", bills.get(0).getType());
		assertEquals("01-01-2012", PublicConstants.SIMPLE_DATE_FORMAT.format(bills.get(0).getStartDate()));
		assertEquals("31-03-2012", PublicConstants.SIMPLE_DATE_FORMAT.format(bills.get(0).getEndDate()));
		assertEquals(1200, bills.get(0).getTotal(), 0.1);

		assertEquals("Gas", bills.get(3).getType());
		assertEquals("Water", bills.get(5).getType());
	}

}

## Shared Bills Application

### Assumptions

* Input file is in CSV comma separated format without column names/headers.

### Instruction on how to run the application

#### Execute the pre-compiled application

* Open terminal (for Linux/Unix OS) or command windows (for Windows OS) in payslip/target folder
* To execute the application with data input files, run: java -jar shared-bills.jar <member_data_file> <bill_data_file> <summary_output_file>
* E.g. java -jar shared-bills.jar members.csv bills.csv bills_summary_2012.txt
